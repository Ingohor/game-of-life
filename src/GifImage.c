//
// Created by grinion on 10.03.16.
//

#include "GifImage.h"

void image_set_title(GifImage* image, int n)
{
    char s[16];
    sprintf( s, "%d", n);

    DrawSetFont (image->d_wand, "Verdana-Bold-Italic" ) ;
    DrawSetFontSize(image->d_wand, 40);
    DrawAnnotation(image->d_wand, 0 , 35, s);
}

void image_fill_box(GifImage* image, int x, int y)
{
    PixelSetColor(image->p_wand, "green");
    DrawSetFillColor(image->d_wand, image->p_wand);
    int i, j;
    for (i = x*20 +1; i < x*20 + 20; ++i)
    {
        for (j = HEIGHT_TOP + y*20 + 1; j < HEIGHT_TOP + y*20 + 1 + 19; ++j)
        {
            DrawPoint(image->d_wand,i,j);
        }
    }

    PopDrawingWand(image->d_wand);
}

void image_create_grid(GifImage* image, int** grid)
{
    PushDrawingWand(image->d_wand);
    PixelSetColor(image->p_wand, "black");
    DrawSetStrokeColor(image->d_wand, image->p_wand);
    DrawSetStrokeWidth(image->d_wand, 1);

    // Draw horizontal lines
    for (int i = HEIGHT_TOP; i <= image->height; i += 20)
        DrawLine(image->d_wand, 0, i, image->weight, i);
    // Draw vertical lines
    for (int i = 0; i <= image->weight; i += 20)
        DrawLine(image->d_wand, i, HEIGHT_TOP, i, image->height);

    PopDrawingWand(image->d_wand);

    for (int i = 0; i < image->n; i++)
    {
        for (int j = 0; j < image->n; j++)
        {
            if(grid[j][i] == 1)
                image_fill_box(image, i, j);
        }
    }
}


GifImage* create_image(int n)
{
    GifImage* image = malloc(sizeof(GifImage));
    if(image == NULL) return NULL;
    MagickWandGenesis();
    image->main_wand = NewMagickWand();
    MagickSetFormat(image->main_wand, "gif");

    image->p_wand = NewPixelWand();
    image->d_wand = NewDrawingWand();

    image->weight = 20 * n;
    image->height = HEIGHT_TOP + 20 * n;

    image->n_frame = 0;
    image->n = n;
    return image;

}
void add_frame(GifImage* image, int** grid, int delay)
{

    MagickWand* rw = NewMagickWand();
    PixelSetColor(image->p_wand,"white");
    MagickNewImage(rw,  image->weight, image->height, image->p_wand);

    image_set_title(image, image->n_frame);
    image_create_grid(image, grid);

    MagickDrawImage(rw, image->d_wand);

    MagickSetImageDelay(rw, delay);

    MagickAddImage(image->main_wand,rw);

    DestroyMagickWand(rw);
    ClearDrawingWand(image->d_wand);
    image->n_frame++;
}
void save_image(GifImage* image, char* filename)
{
    MagickSetImageProperty(image->main_wand, "loop", "1");
    MagickWriteImages(image->main_wand, filename, true);
    /* Tidy up */
    image->main_wand = DestroyMagickWand(image->main_wand);
    image->d_wand = DestroyDrawingWand(image->d_wand);
    image->p_wand = DestroyPixelWand(image->p_wand);
    free(image);
    MagickWandTerminus();
}

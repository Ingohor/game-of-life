#include <getopt.h>
#include <string.h>
#include "Game.h"
#include "GifImage.h"


char* replace_extensions_txt_to_gif(char* str, char* ex)
{
    char *tmp = malloc(strlen(str) + strlen(ex));
    strcpy(tmp, str);
    for (int i = 0; i < strlen(ex); ++i)
    {
        tmp[strlen(str) - 3 + i] = ex[0 + i]; // <<=== This is OK too
    }
    return tmp;
}
int main(int argc, char** argv)
{


    char opt;
    char* openfilename = "data.txt";
    char* savefilename = "new_data.txt";
    int size = 0;
    int n = 10;
    while ((opt = (char) getopt (argc, argv, "i:o:n:s:")) != -1) {
        switch (opt) {
            case 'i':
                openfilename = optarg;
                break;
            case 'o':
                savefilename = optarg;
                break;
            case 'n':
                n = atoi(optarg);
                break;
            case 's':
                size = atoi(optarg);
                break;
            default:                   /* '?' */
             //   fprintf (stderr, usage, progname);
                exit (EXIT_FAILURE);
        }
    }
    if(size > 0)
    {
        FILE* file;
        if((file = fopen(savefilename, "w")) == NULL)
        {
            return EXIT_FAILURE;
        }
        Grid* grid;
        if((grid = create_grid(size)) == NULL)
        {
            return EXIT_FAILURE;
        }
        write_grid(grid, file);
        free_grid(grid);
        fclose(file);
        return EXIT_SUCCESS;
    }

    Game* game = game_load(openfilename);
    if(game == NULL)
    {
        return EXIT_FAILURE;
    }
    GifImage* image = create_image(game->grid->size);
    add_frame(image, game->grid->cells, 200);
    for (int i = 0; i < n; ++i)
    {
        game_next_step(game);
        if(i != n - 1) add_frame(image, game->grid->cells,50);
        else add_frame(image, game->grid->cells,200);
    }
    game_save(game, savefilename);
    savefilename = replace_extensions_txt_to_gif(savefilename, "gif");
    save_image(image, savefilename);
    free_game(game);
    return EXIT_SUCCESS;
}
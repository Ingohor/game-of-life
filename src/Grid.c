//
// Created by Aleksander Granowski
//

#include "Grid.h"
Grid* create_grid(int size)
{
    Grid* grid = malloc(sizeof(Grid));
    grid->size = size;
    grid->cells = malloc(sizeof(*grid->cells) * size);
    for (int i = 0; i < size; i++)
    {
        grid->cells[i] = calloc(size, sizeof(*grid->cells[i]));
    }
    return grid;
}

Grid* read_grid(FILE* in)
{
    if(in == NULL) return NULL;
    int size;
    if(fscanf(in, "%d", &size) != 1)
    {
        return NULL;
    }

    Grid* grid = create_grid(size);
    if(grid == NULL)
    {
        return NULL;
    }
    for(int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if(fscanf(in, "%d", &grid->cells[i][j]) != 1)
            {
                free_grid(grid);
                return NULL;
            }
        }
    }
    return grid;
}
void write_grid(Grid* grid, FILE* out)
{
    fprintf(out, "%d\n", grid->size);
    for(int i = 0; i < grid->size; i++)
    {
        for (int j = 0; j < grid->size; j++)
        {
            if(fprintf(out, "%d ", grid->cells[i][j]) != 1)
            {
            }
        }
        fprintf(out, "\n");
    }
}
void free_grid(Grid* grid)
{
    for (int i = 0; i < grid->size; i++)
    {
        free(grid->cells[i]);
    }
    free(grid->cells);
    free(grid);
    grid = NULL;
}

int get_many_nearby_cells(Grid* grid, int x, int y)
{
    int result = 0;
    for (int i = -1; i <= 1; ++i)
    {
        int ty = y - i;
        if(ty < 0 || grid->size <= ty) continue;

        for (int j = -1; j <= 1; ++j)
        {
            int tx = x - j;
            if(tx < 0 || grid->size <= tx) continue;
            if(tx == x && ty == y) continue;
            if(grid->cells[ty][tx] == 1)
            {
                result += 1;
            }
        }
    }
    return result;
}
//
// Created by Aleksander Granowski
//

#include "Game.h"
Game* game_load(char * filename)
{
    FILE* f = fopen(filename, "rw");
    if(f == NULL)
    {
        return NULL;
    }
    Grid* grid = read_grid(f);
    if(grid == NULL)
    {
        return NULL;
    }
    Game* game = create_game(grid);
    if(game == NULL)
    {
        free_grid(grid);
        return NULL;
    }
    return game;
}
Game* create_game(Grid* grid)
{
    Game* game = malloc(sizeof(Game));
    game->grid = grid;
    return game;
}

void game_next_step(Game* game)
{
    Grid* nextgrid = create_grid(game->grid->size);
    for (int y = 0; y < game->grid->size; ++y)
    {
        for (int x = 0; x < game->grid->size; ++x)
        {
            int n = get_many_nearby_cells(game->grid, x, y);
            if(game->grid->cells[y][x] == 0)
            {
                if(n == 3)
                {
                    nextgrid->cells[y][x] = 1;
                    continue;
                }
            }
            if(game->grid->cells[y][x] == 1)
            {
                if(n < 2 || n > 3)
                {
                    nextgrid->cells[y][x] = 0;
                    continue;
                }
            }
            nextgrid->cells[y][x] = game->grid->cells[y][x];
        }
    }
    Grid* tmp = game->grid;
    game->grid = nextgrid;
    free_grid(tmp);
}
void game_save(Game* game, char* filename)
{
    FILE* file = fopen(filename, "w");
    if(file == NULL)
    {
        return;
    }
    write_grid(game->grid, file);
}
void free_game(Game* game)
{
    free_grid(game->grid);
    free(game);
    game = NULL;
}
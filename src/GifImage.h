//
// Created by grinion on 10.03.16.
//

#ifndef PROJECT_IMAGE_H
#define PROJECT_IMAGE_H
#define HEIGHT_TOP 40
#include <wand/magick-wand.h>
#include <stdbool.h>
typedef struct
{
    DrawingWand* d_wand;
    PixelWand* p_wand;
    MagickWand* main_wand;
    int weight;
    int height;
    int n_frame;
    int n;

} GifImage;
GifImage* create_image(int n);
void add_frame(GifImage* image, int** grid, int delay);
void save_image(GifImage* image, char* filename);
#endif //PROJECT_IMAGE_H

//
// Created by Aleksander Granowski
//

#ifndef GAME_OF_LIFE_GAME_H
#define GAME_OF_LIFE_GAME_H
#include "Grid.h"
typedef struct
{
    Grid* grid;
} Game;
Game* game_load(char* filename);
Game* create_game(Grid* save);
void game_next_step(Game* game);
void game_save(Game* game, char* filename);
void free_game(Game* game);
#endif //GAME_OF_LIFE_GAME_H

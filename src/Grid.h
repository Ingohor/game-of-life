//
// Created by Aleksander Granowski
//

#ifndef GAME_OF_LIFE_GRID_H
#define GAME_OF_LIFE_GRID_H
#include <stdlib.h>
#include <stdio.h>
typedef struct
{
    int size;
    int ** cells;
} Grid;

Grid* create_grid(int size);
Grid* read_grid(FILE* in);
void write_grid(Grid* grid, FILE* out);
void free_grid(Grid* grid);
int get_many_nearby_cells(Grid* grid, int x, int y);
#endif //GAME_OF_LIFE_GRID_H
